// add libs
const knex = require('knex')({
  client: 'pg',
  connection: {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
  }
});

async function handleEntry(entryDataPrev, tableName) {
  if(entryDataPrev.fullname === null) {
    updateBasicData(entryDataPrev, tableName);
    if (entryDataPrev.basicForm.healthInsurance !== null && entryDataPrev.basicForm.healthInsurance.healthCertificateExpirationFile !== null) {
      convertHealthCertFileData(entryDataPrev, tableName);
    }
  } else {
    console.log({'update skipped for ID: ':entryDataPrev.id})
  }
}

async function updateBasicData(entryDataPrev, tableName) {

  let update = convertBasicData(entryDataPrev);
  await knex(tableName)
    .where({id: entryDataPrev.id})
    .update(update)
    .then((entryDataFromDb) => {
      // console.log('===================================================');
      // console.log('updateBasicData');
      // console.log('===================================================');
      console.log({'Updated basic data for:':entryDataPrev.id});
    })
    .catch((error) => {
      console.log({'error on': entryDataPrev.id});
      console.log(error)
    });
}

async function convertHealthCertFileData(entryDataPrev, tableName) {
  await entryDataPrev.basicForm.healthInsurance.healthCertificateFile.forEach((fileData) => {
    // console.log(fileData);
    knex('upload_file_morph')
      .where({upload_file_id: fileData.id})
      .update({
        related_id: entryDataPrev.id,
        related_type: tableName
      })
      .then((entryDataFromDb) => {
        // console.log('===================================================');
        // console.log('updateBasicData');
        // console.log('===================================================');
        console.log({'Updated health cert file for:':entryDataPrev.id});
      })
      .catch((error) => {
        console.log({'error on': entryDataPrev.id});
        console.log(error)
      });
  });

}

function convertBasicData(entryDataPrev) {
  let updatedEntry = {
    fullname: entryDataPrev.basicForm.basicData.fullname,
    email: entryDataPrev.basicForm.basicData.email,
    mobile: entryDataPrev.basicForm.basicData.mobile,
    nationalId: entryDataPrev.basicForm.basicData.nationalId,
    nationalIdExpirationDate: entryDataPrev.basicForm.basicData.nationalIdExpirationDate,
    region: entryDataPrev.basicForm.basicData.region,
    city: entryDataPrev.basicForm.basicData.city,
    district: entryDataPrev.basicForm.basicData.district
  }

  if (entryDataPrev.basicForm.healthInsurance !== null) {
    updatedEntry.healthCertificateNumber = entryDataPrev.basicForm.healthInsurance.healthCertificateNumber;
    updatedEntry.healthCertificateExpirationDate = entryDataPrev.basicForm.healthInsurance.healthCertificateExpirationDate;
  }

  if (entryDataPrev.basicForm.food !== null) {
    updatedEntry.foodTypes = JSON.stringify(entryDataPrev.basicForm.food.types);
    updatedEntry.foodOtherCategories = entryDataPrev.basicForm.food.otherCategories;
  }

  return updatedEntry;
}

module.exports = {
  handleEntry
}
