// add libs
const axios = require('axios');
const {isArray} = require("lodash");

// add apps db handler
const db = require('./db');

// def variables
let url;
if(process.env.TOJJAR_API_ENV === 'local') {
  url = 'http://localhost:1337/';
} else {
  url = 'https://'+process.env.API_URL+'/';
}
// const url = 'http://localhost:1337/';
let table;

axios.post(url+'auth/local', {
  identifier: process.env.NEXT_PUBLIC_USER,
  password: process.env.NEXT_PUBLIC_PASSWORD,
}).then(function (response) {
  // handle success
  // console.log(response.data);
  getDataFromEndpoint(response.data.jwt, 'apps');
  getDataFromEndpoint(response.data.jwt, 'home-based-businesses');
}).catch(function (error) {
  // handle error
  console.log(error);
});
async function getDataFromEndpoint(token, endpoint) {
  await axios.get(url+ endpoint, {
    headers: {
      Authorization: 'Bearer ' + token
    }
  }).then(function (response) {
    let entries = response.data;
    if (isArray(entries)) {
      table = endpoint.replace(/-/g, "_")
      console.log({'starting update for table:':table});
      // handle array of entries
      entries.forEach((entry) => {
        db.handleEntry(entry, table);
      });
    } else {
      // handle single entry
      db.handleEntry(entries, endpoint);
    }
  }).catch(function (error) {
    // handle error
    console.log(error);
  });
}
