module.exports = ({env}) => ({
  sentry: {
    enabled: true,
    resolve: './src/plugins/my-sentry-fork',
    config: {
      dsn: env('TOJJAR_API_SENTRY_DSN'),
      sendMetadata: true,
      myCustomSetting: false,
      traceSampleRate: env('TOJJAR_API_SENTRY_TRACE_SAMPLE_RATE')
    },
    init: {

    }
  },
  email: {
    provider: env('EMAIL_PROVIDER'),
    providerOptions: {
      host: env('EMAIL_SMTP_HOST', 'smtp.example.com'),
      port: env('EMAIL_SMTP_PORT', 587),
      auth: {
        user: env('EMAIL_SMTP_USER'),
        pass: env('EMAIL_SMTP_PASS'),
      },
    },
    settings: {
      defaultFrom: env('EMAIL_ADDRESS_FROM'),
      defaultReplyTo: env('EMAIL_ADDRESS_REPLY'),
      defaultSendTo: env('EMAIL_SEND_TO'),
    },
  },
  upload: {
    provider: 'google-cloud-storage',
    providerOptions: {
      bucketName: env('GCS_BUCKET'),
      publicFiles: false,
      uniform: true,
      basePath: '',
      skipCheckBucket: true,
      baseUrl: `https://storage.googleapis.com/${env('GCS_BUCKET')}`
    }
  }
});
