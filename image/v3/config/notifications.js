module.exports = ({env}) => {
  return {
      booths: {
        contact: env('EMAIL_SEND_BOOTHS_CONTACT_NOTIFICATIONS_TO'),
        notifications: env('EMAIL_SEND_BOOTHS_NOTIFICATIONS_TO')
      },
      apps: {
        contact: env('EMAIL_SEND_APPS_CONTACT_NOTIFICATIONS_TO'),
        notifications: env('EMAIL_SEND_APPS_NOTIFICATIONS_TO')
      }
  };
};
