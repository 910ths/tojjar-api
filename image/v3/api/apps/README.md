# Fields

* `basicForm` - required object for the `basicForm` component
  * `basicData` - required object for the `basicData` component
    * `fullname` - name of the person that registers
    * `email` - email for contact
    * `mobile` - mobile number for contact
    * `nationalId` - the NIC number
    * `nationalIdExpirationDate` - expiration date of the NIC number
    * `region` - region of work
    * `city` - city of work
    * `district` - district of work
  * `food` - required object for the `food` component
    * `types` - json input:
  * `healthInsurance` - optional object for the `healthInsurance` component
    * `healthCertificationNumber` - health insurance certificate number
    * `healthCertificationExpirationDate` - health insurance certificate expiration date
    * `healthCertificationFile` - health insurance certificate file proof (jpg, png, pdf, whatever)
* `logo` - logo file
* `isOrderReadyWithinTwnetyMinutes` - boolean field
* `agreeToPlaceSign` - boolean field

# Usage

## Sending request

endpoint: `POST /home-based-businesses`

Important! Data should be sent with `multipart/form-data`!

### Example request with health certificate:

parameter `data` (that should send with `JSON.stringify(...)`) with "Content Type" `application/json`
```json
{
  "isOrderReadyWithinTwentyMinutes": true,
  "agreeToPlaceSign": true,
  "basicForm": {
    "basicData": {
      "fullname": "A",
      "email": "piotr@bozetka.net",
      "mobile": 123456789,
      "nationalId": 1234567890,
      "nationalIdExpirationDate": "2021-11-30T11:00:00.000Z",
      "region": "A",
      "city": "A",
      "district": "A"
    },
    "food": {
      "types": [
        "Local",
        "Korean",
        "Asian",
        "Whatever we want"
      ]
    },
    "healthInsurance": {
      "healthCertificateNumber": "1234567890",
      "healthCertificateExpirationDate": "2021-11-29T11:00:00.000Z"
    }
  }
}
```
parameter `files.basicForm.healthInsurance.healthCertificateFile`:
```
file you want to upload
```

parameter `files.logo`:
```
file you want to upload
```


### Example request without health certificate:

parameter `data` (that should send with `JSON.stringify(...)`) with "Content Type" `application/json`
```json
{
  "isOrderReadyWithinTwentyMinutes": true,
  "agreeToPlaceSign": true,
  "basicForm": {
    "basicData": {
      "fullname": "A",
      "email": "piotr@bozetka.net",
      "mobile": 123456789,
      "nationalId": 1234567890,
      "nationalIdExpirationDate": "2021-11-30T11:00:00.000Z",
      "region": "A",
      "city": "A",
      "district": "A"
    },
    "food": {
      "types": [
        "Local",
        "Korean",
        "Asian",
        "Whatever we want"
      ]
    }
  }
}
```

parameter `files.logo`:
```
file you want to upload
```
