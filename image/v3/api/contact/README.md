# Fields
* `email` - Email for contact OR
* `phone` - Mobile for contact
* `question` - the question being asked
* `source` - the source from where the question is being asked the value should be either `apps` or `booths`

## Usage
### Sending request

endpoint: `POST /booth-companies`

###Example request bodys:

#### Email contact from booths
```json
{
  "email" : "piotr.bozetka@tsh.io",
  "source": "booths",
  "question": "Why is the sea blue?"
}
```
#### Phone contact from booths
```json
{
    "phone" : "1234567890",
    "source": "booths",
    "question": "Why is the sea blue?"
}
```
#### Email contact from apps
```json
{
    "email" : "piotr.bozetka@tsh.io",
    "source": "apps",
    "question": "Why is the sky green?"
}
```
#### Phone contact from apps
```json
{
    "phone" : "1234567890",
    "source": "apps",
    "question": "Why is the sky green?"
}
```
