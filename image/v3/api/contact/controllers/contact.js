'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {parseMultipartData, sanitizeEntity} = require('strapi-utils');

module.exports = {
  async create(ctx) {

    let entity;
    let sendTo;

    if (ctx.is('multipart')) {
      const {data, files} = parseMultipartData(ctx);
      entity = await strapi.services.contact.create(data, {files});
    } else {
      entity = await strapi.services.contact.create(ctx.request.body);
    }

    entity = sanitizeEntity(entity, {model: strapi.models.contact});
    sendTo = strapi.config.get('notifications.'+entity.source+'.contact');

    if(sendTo !== '') {
      // send an email by using the email plugin
      await strapi.plugins['email'].services.email.send({
        to: sendTo.split(','),
        subject: 'New question from Tojjar contact form',
        text: `
          <p>There is a new question with id #${entity.id}.</p>

          <p>Source: ${entity.source}</p>
          <p>Contact email: ${entity.email}</p>
          <p>Contact phone: ${entity.phone}</p>
          <p>
            Question:<br/>
            ${entity.question}
          </p>
        `,
      });
    }

    return entity;
  },
};
