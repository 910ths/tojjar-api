module.exports = () => {
  return async (ctx, next) => {
    console.log(ctx.request.url)
    await next();
    if (ctx.request.url.includes("upload/files")) {
      if (ctx.response.body && typeof ctx.response.body === "object") {
        for (let file of ctx.response.body.results) {
          console.log((file));
          if (file.url.includes(".googleapis.")) delete file.updatedAt
        }
      }
    }
  };
};
