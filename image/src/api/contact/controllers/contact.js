'use strict';

/**
 * contact controller
 */
const {parseMultipartData, sanitizeEntity, sanitize} = require('@strapi/utils');

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::contact.contact', ({strapi}) => ({
  async create(ctx) {

    let entity;
    let sendTo;

    if (ctx.is('multipart')) {
      const {data, files} = parseMultipartData(ctx);
      entity = await strapi.entityService.create('api::contact.contact', {
        data,
        files
      })
    } else {
      // entity = await strapi.services.contact.create(ctx.request.body);
      entity = await strapi.entityService.create('api::contact.contact', {data: ctx.request.body})

    }

    // entity = sanitizeEntity(entity, {model: strapi.models.contact});
    const schema = strapi.getModel('api::contact.contact');
    entity = await sanitize.contentAPI.output(entity, schema);
    sendTo = strapi.config.get('notifications.'+entity.source+'.contact');

    if(sendTo) {
      // send an email by using the email plugin
      await strapi.plugins['email'].services.email.send({
        to: sendTo.split(','),
        subject: 'New question from Tojjar contact form',
        text: `
          <p>There is a new question with id #${entity.id}.</p>

          <p>Source: ${entity.source}</p>
          <p>Contact email: ${entity.email}</p>
          <p>Contact phone: ${entity.phone}</p>
          <p>
            Question:<br/>
            ${entity.question}
          </p>
        `,
      });
    }

    return entity;
  },

}));
