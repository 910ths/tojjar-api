'use strict';

const {parseMultipartData, sanitize} = require("@strapi/utils");
const {isEmpty} = require("lodash");
/**
 * app controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::app.app', ({strapi}) => ({
  async create(ctx) {

    let entity;
    let sendTo = strapi.config.get('notifications.apps.contact');

    if (ctx.is('multipart')) {
      const {data, files} = parseMultipartData(ctx);
      let fields = [];
      let isMoreThanTenDays = false;
      try {
        // .where({email: data.email}).orWhere({mobile: data.mobile});
        const results = await strapi.entityService.findMany('api::app.app', { filters: {
            $or: [
              {
                email: data.email
              },
              {mobile: data.mobile}
            ],
          }})
        if (!isEmpty(results)) {
          results.forEach((result) => {
            if (result.mobile === data.mobile) {
              if (!fields.includes('mobile')) {
                fields.push('mobile');
              }
            }
            if (result.email === data.email) {
              if (!fields.includes('email')) {
                fields.push('email');
              }
            }

            if (!isEmpty(fields)) {
              let entryDate = new Date(result.created_at);
              let now = new Date();
              let resultInDays = dateDiffInDays(entryDate, now);
              isMoreThanTenDays = (resultInDays >= 10);
            }
          })

          if (!isEmpty(fields)) {
            throw 'found duplicated content'
          }
        }
      } catch (error) {
        ctx.send({
          status: 409,
          error: 'validation.user-exists',
          fields: fields,
          isMoreThanTenDays: isMoreThanTenDays
        }, 409);

        return;
      }
      entity = await strapi.entityService.create('api::app.app', {
        data,
        files
      })
    } else {
      entity = await strapi.entityService.create('api::app.app', {data: ctx.request.body})

    }
    const schema = strapi.getModel('api::app.app');
    entity = await sanitize.contentAPI.output(entity, schema)
    if (sendTo) {
      // send an email by using the email plugin
      await strapi.plugins['email'].services.email.send({
        to: sendTo.split(','),
        subject: 'New app entry in Tojjar Apps',
        text: `
          <p>There is a new app entry with id #${entity.id}.</p>
        `,
      });
    }
    ctx.created(entity)
    return entity;
  },
}));

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}
