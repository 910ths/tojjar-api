'use strict';

/**
 * home-based-business router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::home-based-business.home-based-business');