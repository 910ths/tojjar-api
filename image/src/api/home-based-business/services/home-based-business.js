'use strict';

/**
 * home-based-business service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::home-based-business.home-based-business');