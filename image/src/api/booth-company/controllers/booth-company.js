'use strict';

/**
 * booth-company controller
 */
const {isEmpty} = require("lodash");
const {sanitize} = require("@strapi/utils");

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::booth-company.booth-company', ({strapi}) => ({
  async create(ctx) {

    let entity;

    if (!ctx.is('multipart')) {

      let data = ctx.request.body;
      let fields = [];
      let isMoreThanTenDays = false;
      try {
      // .where({email: data.email}).orWhere({mobileNumber: data.mobileNumber})
        const results = await strapi.entityService.findMany('api::booth-company.booth-company', { filters: {
            $or: [
              {
                email: data.email
              },
              {mobileNumber: data.mobileNumber}
            ],
          }});
        if (!isEmpty(results)) {
          results.forEach((result) => {
            if (result.mobileNumber === data.mobileNumber) {
              if (!fields.includes('mobileNumber')) {
                fields.push('mobileNumber');
              }
            }
            if (result.email === data.email) {
              if (!fields.includes('email')) {
                fields.push('email');
              }
            }

            if (!isEmpty(fields)) {
              let entryDate = new Date(result.created_at);
              let now = new Date();
              let resultInDays = dateDiffInDays(entryDate, now);
              isMoreThanTenDays = (resultInDays >= 10);
            }
          })

          if (!isEmpty(fields)) {
            throw 'found duplicated content'
          }
        }
      } catch (error) {
        ctx.send({
          status: 409,
          error: 'validation.user-exists',
          fields: fields,
          isMoreThanTenDays: isMoreThanTenDays
        }, 409);

        return;
      }

      entity = await strapi.entityService.create('api::booth-company.booth-company', {data: ctx.request.body})
      const schema = strapi.getModel('api::booth-company.booth-company');
      entity = await sanitize.contentAPI.output(entity, schema);
    } else {
      const {data, files} = parseMultipartData(ctx);
      let fields = [];
      let isMoreThanTenDays = false;
      try {
        const results = await strapi.entityService.findMany('api::booth-company.booth-company', { filters: {
            $or: [
              {
                email: data.email
              },
              {mobileNumber: data.mobileNumber}
            ],
          }});

        if (!isEmpty(results)) {
          results.forEach((result) => {
            if (result.mobileNumber === data.mobileNumber) {
              if (!fields.includes('mobileNumber')) {
                fields.push('mobileNumber');
              }
            }
            if (result.email === data.email) {
              if (!fields.includes('email')) {
                fields.push('email');
              }
            }

            if (!isEmpty(fields)) {
              let entryDate = new Date(result.created_at);
              let now = new Date();
              let resultInDays = dateDiffInDays(entryDate, now);
              isMoreThanTenDays = (resultInDays >= 10);
            }
          })

          if (!isEmpty(fields)) {
            throw 'found duplicated content'
          }
        }
      } catch (error) {
        ctx.send({
          status: 409,
          error: 'validation.user-exists',
          fields: fields,
          isMoreThanTenDays: isMoreThanTenDays
        }, 409);

        return;
      }

      entity = await strapi.entityService.create('api::booth-company.booth-company', {
        data,
        files
      })
    }
    const schema = strapi.getModel('api::booth-company.booth-company');
    entity = await sanitize.contentAPI.output(entity, schema);

    return entity;
  },

}));

const _MS_PER_DAY = 1000 * 60 * 60 * 24;

function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

