'use strict';

/**
 * booth-company router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::booth-company.booth-company');