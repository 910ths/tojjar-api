# Fields
* `businessName` - Name of the business
* `fullname` - Name of the person registering
* `email` - Email for contact
* `mobileNumber` - Mobile for contact
* `booths` - an array of objects with the data of the booths. Minimal 1, maximum 10 entries.
  * `region` - Region of the booth
  * `city` - City of the booth
  * `address` - Address of the booth
  * `employees` -  Choice array, only one option to choose from:
    * `upTo50`
    * `upTo100`
    * `upTo200`
    * `upTo500`
    * `over500`

# Usage

## Sending request

endpoint: `POST /booth-companies`

example request body:
```json
{
    "businessName": "TSH",
    "fullname": "Peter B",
    "email": "piotr.bozetka@tsh.io",
    "mobileNumber": 123456789,
    "created_at": "2021-11-09T12:10:19.125Z",
    "updated_at": "2021-11-09T12:10:19.125Z",
    "booths": [
        {
            "region": "C",
            "city": "C",
            "address": "C",
            "employees": "upTo50"
        },
        {
            "region": "D",
            "city": "D",
            "address": "D",
            "employees": "upTo100"
        }
    ]
}
```
