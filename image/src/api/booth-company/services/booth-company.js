'use strict';

/**
 * booth-company service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::booth-company.booth-company');