# Fields

* `targetPage` - the page that the question should be shown (either `booths` or `apps`)
* `targetUser` - the type of user that the question should be shown (either `individual` or `partners`)
* `question` - "question" entry
* `answer` - "answer" to the question
  
# Usage:

## GENERAL

Show all entries
  `GET /faqs`

Expected example response:
```json
[
    {
        "id": 1,
        "targetPage": "booths",
        "targetUser": "individual",
        "question": "Question 1",
        "answer": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dignissim dolor non pulvinar pharetra. Donec ex ipsum, fringilla quis risus eu, blandit hendrerit neque. Nullam semper tortor at mi efficitur vulputate. Morbi tellus libero, sollicitudin ac libero in, tempor rhoncus nulla. Vestibulum justo dolor, fringilla et arcu vel, cursus maximus nisi. Quisque vel sem arcu. Duis porttitor semper tellus finibus egestas. Integer pharetra lectus diam, sed mattis tortor egestas quis. Pellentesque sit amet lacus eu nisl laoreet dignissim. Pellentesque ornare egestas libero efficitur dictum. Vivamus laoreet ex feugiat nisi rhoncus, eu imperdiet sapien accumsan. Donec dignissim magna scelerisque euismod euismod. Praesent finibus ipsum placerat, suscipit augue ut, ullamcorper purus. Mauris rhoncus sodales mauris sed auctor. Nam tempus diam eget accumsan dictum.",
        "created_at": "2021-11-05T15:55:09.731Z",
        "updated_at": "2021-11-05T15:55:09.731Z"
    },
    {
        "id": 2,
        "targetPage": "booths",
        "targetUser": "partner",
        "question": "Question 2",
        "answer": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dignissim dolor non pulvinar pharetra. Donec ex ipsum, fringilla quis risus eu, blandit hendrerit neque. Nullam semper tortor at mi efficitur vulputate. Morbi tellus libero, sollicitudin ac libero in, tempor rhoncus nulla. Vestibulum justo dolor, fringilla et arcu vel, cursus maximus nisi. Quisque vel sem arcu. Duis porttitor semper tellus finibus egestas. Integer pharetra lectus diam, sed mattis tortor egestas quis. Pellentesque sit amet lacus eu nisl laoreet dignissim. Pellentesque ornare egestas libero efficitur dictum. Vivamus laoreet ex feugiat nisi rhoncus, eu imperdiet sapien accumsan. Donec dignissim magna scelerisque euismod euismod. Praesent finibus ipsum placerat, suscipit augue ut, ullamcorper purus. Mauris rhoncus sodales mauris sed auctor. Nam tempus diam eget accumsan dictum.",
        "created_at": "2021-11-05T16:27:49.260Z",
        "updated_at": "2021-11-05T16:27:49.260Z"
    },
    {
        "id": 3,
        "targetPage": "apps",
        "targetUser": "individual",
        "question": "Question 3",
        "answer": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dignissim dolor non pulvinar pharetra. Donec ex ipsum, fringilla quis risus eu, blandit hendrerit neque. Nullam semper tortor at mi efficitur vulputate. Morbi tellus libero, sollicitudin ac libero in, tempor rhoncus nulla. Vestibulum justo dolor, fringilla et arcu vel, cursus maximus nisi. Quisque vel sem arcu. Duis porttitor semper tellus finibus egestas. Integer pharetra lectus diam, sed mattis tortor egestas quis. Pellentesque sit amet lacus eu nisl laoreet dignissim. Pellentesque ornare egestas libero efficitur dictum. Vivamus laoreet ex feugiat nisi rhoncus, eu imperdiet sapien accumsan. Donec dignissim magna scelerisque euismod euismod. Praesent finibus ipsum placerat, suscipit augue ut, ullamcorper purus. Mauris rhoncus sodales mauris sed auctor. Nam tempus diam eget accumsan dictum.",
        "created_at": "2021-11-05T16:28:01.682Z",
        "updated_at": "2021-11-05T16:28:01.682Z"
    }
]
```

Show single entries
  `/faqs/{number}` for example `/faqs/1`

## BOOTHS

Show all entries that should be shown in tojjar-booths
  `faqs?targetPage=booths`

Show all entries in tojjar-booths that should be shown for individual users
`faqs?targetPage=booths&targetUser=individual`

Show all entries in tojjar-booths that should be shown for partner users
`faqs?targetPage=booths&targetUser=individual`

## APPS

Show all entries that should be shown in tojjar-apps
  `faqs?targetPage=apps`

Show all entries in tojjar-apps that should be shown for individual users
  `faqs?targetPage=apps&targetUser=individual`

Show all entries in tojjar-apps that should be shown for partner users
  `faqs?targetPage=apps&targetUser=individual`
