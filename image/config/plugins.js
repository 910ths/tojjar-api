module.exports = ({ env }) => ({
  upload: {
    config: {
      provider: '@strapi-community/strapi-provider-upload-google-cloud-storage',
      providerOptions: {
        bucketName: env("GCS_BUCKET", ''),
        publicFiles: false,
        uniform: false,
        basePath: '',
        skipCheckBucket: true,
        baseUrl: `https://storage.googleapis.com/${env('GCS_BUCKET')}`
      },
    }
  },
  sentry: {
    enabled: true,
    config: {
      dsn: env('TOJJAR_API_SENTRY_DSN'),
      sendMetadata: true,
      init: {
        environment: env('TOJJAR_API_ENV'),
        traceSampleRate: env('TOJJAR_API_SENTRY_TRACE_SAMPLE_RATE')
      }
    },
  },
  email: {
    provider: env('EMAIL_PROVIDER'),
    providerOptions: {
      host: env('EMAIL_SMTP_HOST', 'smtp.example.com'),
      port: env('EMAIL_SMTP_PORT', 587),
      auth: {
        user: env('EMAIL_SMTP_USER'),
        pass: env('EMAIL_SMTP_PASS'),
      },
    },
    settings: {
      defaultFrom: env('EMAIL_ADDRESS_FROM'),
      defaultReplyTo: env('EMAIL_ADDRESS_REPLY'),
      defaultSendTo: env('EMAIL_SEND_TO'),
    },
  },
});
