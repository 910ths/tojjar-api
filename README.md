# Tojjar API

###Repository: [https://bitbucket.org/910ths/tojjar-api/src](https://bitbucket.org/910ths/tojjar-api/src)
####Main developer: Peter Bozetka [piotr.bozetka@tsh.io](mailto:piotr.bozetka@tsh.io)

Main notable stack:
* Docker
* Strapi
* AWS S3 (for file uploads)
* Nodemailer (for email usage)
* Sentry (for catchings bugs and errors)

## 1. How to run locally in development mode?

1. `git clone git@bitbucket.org:910ths/tojjar-api.git`
2. copy `.env.example` to a `.env` file and put your own data
3. `make build-image`
4. `make setup`
5. `make start`
6. go to `http://localhost:1337` (make sure that the application started in development mode)
7. create a new super admin account
8. go to `http://localhost:1337/admin` and login

## 2. How to run in production mode?

In your .env change the line:
```
COMPOSE_FILE=compose/00-base.yml:compose/01-expose.yml:compose/99-dev.yml
```
to
```
COMPOSE_FILE=compose/00-base.yml:compose/01-expose.yml
```
and you should be good to go.

## 3. How to use the api on your local environment

1. Create a new role for example `tojjar-spa-role` and for development phase please mark all checkpoints for all endpoints
2. Create a new user for example `tojjar-spa-user` with and email and password of your choice. Mark the `Confirmed` to `On`, assign the role `tojjar-spa-role` to the user and finally click save.
3. Import to `Postman` the collection and env variables from the folder `./postmanCollections`
4. Refer to bellow readme's on how to use the endpoints:
   1. Tojjar Apps [README.md](./image/api/apps/README.md)
   2. Tojjar Booths / Company [README.md](./image/api/booth-company/README.md)
   3. Tojjar Booths / Home Based Businesses (HBB) [README.md](./image/api/home-based-businesses/README.md)
   4. FAQ [README.md](./image/api/faqs/README.md)
   5. Contact Us / Send us a message [README.md](./image/api/contact/README.md)

If there are any changes to the usage, please update the README files.

Note that in the postman env variables collections there is a "Tojjar staging" file that refers to the staging env that is setup at the time of writing this documentation. Feel free to use it.

## 4. How to deploy app to staging/uat/production?

1. Go to [https://bitbucket.org/910ths/tojjar-api/addon/pipelines/home](https://bitbucket.org/910ths/tojjar-api/addon/pipelines/home)
2. Click `Run pipeline`
3. Select a branch
4. After that select the pipeline with prefix `custom: deploy-` and select staging/uat/production
5. Click run
6. Grab a coffee
7. App should be deployed
