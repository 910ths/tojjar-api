version: '3'
vars:
  TAG: latest
  ENV: staging
  KUBECTL_VERSION: v1.18.8
  HELM_VERSION: v3.3.0
  VAULT_VERSION: 1.5.3
  AWS_REGION: eu-west-1
  DOCKER_REGISTRY: 813349298303.dkr.ecr.eu-west-1.amazonaws.com
  DOCKER_IMAGE_NAME: tojjar/api
  SONAR_TOKEN: example_token
  VAULT_ADDR: https://vault.k8s.910ths.sa
env:
  PATH: "{{.PWD}}/bin:{{.PATH}}"
tasks:
  default:
    cmds:
      - task -l
  kubectl:install:
    desc: install kubectl
    cmds:
      - curl -LO https://storage.googleapis.com/kubernetes-release/release/{{.KUBECTL_VERSION}}/bin/{{OS}}/{{ARCH}}/kubectl
      - chmod +x ./kubectl
    status:
      - test -f kubectl
  helm:install:
    desc: install helm
    cmds:
      - wget -qO- https://get.helm.sh/helm-{{.HELM_VERSION}}-{{OS}}-{{ARCH}}.tar.gz | tar xvz -C ./
      - mv {{OS}}-{{ARCH}}/helm ./
      - chmod 755 helm
      - rm -rf {{OS}}-{{ARCH}}
    status:
      - test -f helm
  awscli:install:
    desc: install awsli
    cmds:
      - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
      - unzip awscliv2.zip
      - ./aws/install
      - rm awscliv2.zip
  vault:install:
    desc: install vault
    cmds:
      - curl "https://releases.hashicorp.com/vault/{{.VAULT_VERSION}}/vault_{{.VAULT_VERSION}}_{{OS}}_{{ARCH}}.zip" -o "vault.zip"
      - unzip vault.zip
      - rm vault.zip
    status:
      - test -f vault
  docker:login:
    desc: Login to AWS ECR
    cmds:
      - "aws ecr get-login-password --region {{.AWS_REGION}} | docker login --username AWS --password-stdin {{.DOCKER_REGISTRY}}"
  docker:build:
    desc: build docker images
    deps:
      - docker:login
    cmds:
      - "docker build -t {{.DOCKER_REGISTRY}}/{{.DOCKER_IMAGE_NAME}}:{{.TAG}} ./image/"
  docker:push:
    desc: push docker images to aws registry
    deps:
      - task: docker:build
    cmds:
      - "docker push {{.DOCKER_REGISTRY}}/{{.DOCKER_IMAGE_NAME}}:{{.TAG}}"
  docker:security-scan:
    desc: print security scan results
    cmds:
      - "aws ecr wait image-scan-complete --repository-name {{.DOCKER_IMAGE_NAME}} --image-id imageTag={{.TAG }}"
      - "aws ecr describe-image-scan-findings --repository-name {{.DOCKER_IMAGE_NAME}} --image-id imageTag={{.TAG }} --no-paginate --output table"
  sonarqube:analyze:
    cmds:
      - docker run --rm -e SONAR_TOKEN={{.SONAR_TOKEN}} -v $PWD:/usr/src sonarsource/sonar-scanner-cli
  # Kubernetes deployments
  vault:write:
    desc: write config to vault
    cmds:
      - task: vault:install
      - ./vault kv put 910ths/apps/tojjar-api/env/{{ .ENV }} values.yaml=@deploy.yaml
  vault:read:
    desc: get config from vault
    cmds:
      - task: vault:install
      - ./vault kv get -field values.yaml 910ths/apps/tojjar-api/env/{{ .ENV }}  > deploy.yaml
      - ./vault kv get -field kubeconfig 910ths/infrastructure/tojjar/env/{{ .ENV }}  > kubeconfig
  deploy:
    desc: deploy tojjar app
    cmds:
      - task: helm:install
      - task: vault:read
      - ./helm dependency update ./deployment/tojjar-api
      - ./helm upgrade --kubeconfig=./kubeconfig -i tojjar-api-{{ .ENV }} --namespace tojjar-{{ .ENV }} -f deploy.yaml --create-namespace --set image.tag={{ .TAG }} ./deployment/tojjar-api
  uninstall:
    desc: uninstall
    cmds:
      - ./helm uninstall --kubeconfig=./kubeconfig  --namespace tojjar-{{ .ENV }} tojjar-api-{{ .ENV }}
