#!/usr/bin/env make

default: build-image

start:
	docker-compose start
stop:
	docker-compose stop
down:
	docker-compose down -v --remove-orphans || true
build-image:
	docker build --tag 813349298303.dkr.ecr.eu-west-1.amazonaws.com/tojjar/api:unreleased ./image/
setup:
	docker-compose up -d --remove-orphans
logs:
	docker-compose logs --timestamps --tail 25 --follow
release:
	echo "Hope you did not forget to export variables in .env"
	git tag -a $(version) -m $(version)
	git push origin $(version)
	$(eval IMAGE := ${DOCKER_REPOSITORY}:$(version))
	docker-compose build --no-cache http_server
	docker tag ${DOCKER_REPOSITORY}:unreleased ${IMAGE}
release-push:
	$(eval IMAGE := ${DOCKER_REPOSITORY}:$(version))
	$(aws ecr get-login --no-include-email --region eu-west-1)
	docker push ${IMAGE}
bash:
	docker-compose exec http_server bash
